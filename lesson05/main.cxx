#include "./engine/context.hxx"
#include "./engine/renderer.hxx"

int main(int, char **, char **) {
  context_SDL context;
  context.create_gl_window("Test", 500, 500);
  // use open gl 3.0
  context.create_gl_context(3, 0);
  gl_renderer renderer;
  context.set_renderer(renderer);
  context.render();
  return 0;
}
