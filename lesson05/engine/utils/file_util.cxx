#include "file_util.hxx"
#include <filesystem>
#include <fstream>

#include <algorithm>
#include <sstream>

char *parce_shader(const std::string &path) {
  std::filesystem::path vertex_shader_src{path};
  std::ifstream sh;
  sh.exceptions(std::ifstream::badbit | std::ifstream::failbit);
  try {
    sh.open(vertex_shader_src);
    std::stringstream src;
    src << sh.rdbuf();
    std::string vsrc{src.str()};
    std::string_view s{vsrc};
    char *ch = new char[s.length() + 1];
    s.copy(ch, s.length() + 1);
    return ch;
  } catch (const std::exception &e) {
    std::clog << e.what() << std::endl;
    throw std::runtime_error(std::string("Error loading shader ") +
                             path.data());
  }
}

std::vector<triangle> *parce_vertexes(const std::string &path) {
  std::vector<triangle> *triangles = new std::vector<triangle>();

  std::ifstream file(path);

  triangle tr;
  file >> tr;
  triangles->push_back(tr);
  file >> tr;
  triangles->push_back(tr);
  file >> tr;
  return triangles;
}
