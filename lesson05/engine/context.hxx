#pragma once

#include <SDL.h>
#include <iostream>

#include "renderer.hxx"

class context_SDL {
private:
  SDL_Window *context_window = nullptr;
  gl_renderer *renderer = nullptr;

public:
  context_SDL() {
    int version_check_status = check_SDL_version();
    validate_status(version_check_status);
    int init_SDL_status = init_SDL();
    validate_status(init_SDL_status);
  }
  ~context_SDL() {
    // TODO: deiniting resources
  }
  void create_gl_window(const char * /*title*/, int /*height*/, int /*width*/);
  void create_gl_context(int /*gl_major_version*/, int /*gl_minor_version*/);
  // beging rendering here (if initialization is OK)
  void render();

  // setters
  void set_renderer(gl_renderer &renderer) { this->renderer = &renderer; }

private:
  int check_SDL_version();
  int check_GL_version(int /*gl_major_version*/, int /*gl_minor_version*/);
  int init_SDL();
  void validate_status(int status);
};

inline std::ostream &operator<<(std::ostream &ostream,
                                const SDL_version &version) {
  ostream << static_cast<int>(version.major) << "."
          << static_cast<int>(version.minor) << "."
          << static_cast<int>(version.patch);
  return ostream;
}
