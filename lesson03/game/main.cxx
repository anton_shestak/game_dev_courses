#include <engine.hxx>
#include <iostream>

int main(int, char**, char**){
   Engine engine;

   int status = engine.init();
   if(status == EXIT_FAILURE){
       std::cerr << "SDL init fail" << std::endl;
       return  status;
   }

   bool is_valid = true;
   while(is_valid){
       is_valid = engine.handle_event();
   }
   engine.destroy();
   return EXIT_SUCCESS;
}
