#pragma once

#include <SDL_video.h>
#include <SDL_keycode.h>

class Engine{
private:
    SDL_Window *window;
public:
    int init();
    bool handle_event();
    void destroy();
    void render();
};


void handle_key(SDL_Keycode, bool);
