#include "engine.hxx"

#include <SDL.h>
#include <iostream>

int Engine::init(){
    int init_resul = SDL_Init(SDL_INIT_VIDEO);
    if (init_resul != 0){
        std::cerr << "Sdl init error: " << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    Engine::window = SDL_CreateWindow("Game", 100, 100, 640, 480, SDL_WINDOW_SHOWN);
    if (window == nullptr){
        std::cerr << "Window init error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void Engine::destroy(){
    if(Engine::window!=nullptr){
        SDL_DestroyWindow(Engine::window);
        SDL_Quit();
    }
}


bool Engine::handle_event(){
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    handle_key(event.key.keysym.sym, true);
                    break;
                case SDL_KEYUP:
                    handle_key(event.key.keysym.sym, false);
                    break;
                case SDL_QUIT:
                    return false;
            }
        }
        return true;
}

void handle_key(SDL_Keycode key, bool is_down){
    switch (key) {
        case SDLK_z:
        if(is_down){
            std::cout<< "Z" << std::endl;
        }
            break;
        case SDLK_SLASH:
        if(is_down){
            std::cout << "Slash" << std::endl;
        }
            break;
    }
}
