#include "./render_utils/primitives.hxx"
#include "./sdl_canvas/sdl_canvas.hxx"
#include "cstdlib"

#include <cmath>
#include <iostream>

vertex transformed_vertex(size_t, size_t, int, int, int);

double interpolate(const double f0, const double f1, const double t) {
  if (f0 < f1) {
    return f0 + (f1 - f0) * t;
  } else {
    return f0 - (f0 - f1) * t;
  }
}

struct renderer : public i_renderer {
  virtual void on_gl_created(sdl_canvas *canvas);
  void on_frame_draw(sdl_canvas *canvas, int width, int height);
  virtual void on_gl_destroyed();
} grid_triangle_renderer;

int main(int, char **, char **) {
  sdl_canvas canvas(500, 500);
  canvas.set_renderer(grid_triangle_renderer);
  canvas.render();
  return 0;
}

color generate_color(size_t i, size_t j) {
  color c;
  if (i % 2 == 0) {
    c = {255, 0, 0};
  }
  return c;
}

void generate_triangles(int w, int h, int count_in_one_line,
                        std::vector<vertex> &vertexes) {
  double max_x = w - 1;
  double max_y = h - 1;

  size_t x_step = static_cast<size_t>(max_x / count_in_one_line);
  size_t y_step = static_cast<size_t>(max_y / count_in_one_line);
  using namespace std;
  for (size_t i = 0; i < count_in_one_line - 1; i++) {
    for (size_t j = 0; j < count_in_one_line - 1; j++) {

      size_t x0 = i * x_step;
      size_t y0 = j * y_step;
      color c0 = generate_color(x0, y0);
      vertex v0 = transformed_vertex(x0, y0, c0.r, c0.g, c0.b);

      size_t x1 = i * x_step;
      size_t y1 = (j + 1) * y_step;
      color c1 = generate_color(x1, y1);
      vertex v1 = transformed_vertex(x1, y1, c1.r, c1.g, c1.b);

      size_t x2 = (i + 1) * x_step;
      size_t y2 = j * y_step;
      color c2 = generate_color(x2, y2);
      vertex v2 = transformed_vertex(x2, y2, c2.r, c2.g, c2.b);

      vertexes.push_back(v0);
      vertexes.push_back(v1);
      vertexes.push_back(v2);

      size_t x3 = (i + 1) * x_step;
      size_t y3 = (j + 1) * y_step;
      color c3 = generate_color(x3, y3);
      vertex v3 = transformed_vertex(x3, y3, c3.r, c3.g, c3.b);

      vertexes.push_back(v1);
      vertexes.push_back(v2);
      vertexes.push_back(v3);
    }
  }
}

void generate_vertexes(int w, int h, size_t count_in_one_line,
                       std::vector<vertex> &vertexes) {
  double max_x = w - 1;
  double max_y = h - 1;

  size_t x_step = static_cast<size_t>(max_x / count_in_one_line);
  size_t y_step = static_cast<size_t>(max_y / count_in_one_line);
  using namespace std;
  for (size_t i = 0; i < count_in_one_line; i++) {
    double th = static_cast<double>(i) / (count_in_one_line);
    int rh = static_cast<int>(interpolate(250, 135, th));
    int gh = static_cast<int>(interpolate(250, 206, th));
    int bh = static_cast<int>(interpolate(210, 250, th));
    for (size_t j = 0; j < count_in_one_line; j++) {
      double tv = static_cast<double>(j) / (count_in_one_line);
      size_t x0 = i * x_step;
      size_t y0 = j * y_step;
      int r = static_cast<int>(interpolate(rh, 25, tv));
      int g = static_cast<int>(interpolate(gh, 25, tv));
      int b = static_cast<int>(interpolate(bh, 89, tv));
      vertexes.push_back(transformed_vertex(x0, y0, r, g, b));
    }
  }
}

void generate_indexes(int count, std::vector<size_t> &indexes) {
  for (int i = 0; i < count - 1; i++) {
    for (int j = 0; j < count - 1; j++) {
      uint8_t v0 = static_cast<uint8_t>(i * count + j);
      uint8_t v1 = static_cast<uint8_t>(i * count + j + 1);
      uint8_t v2 = static_cast<uint8_t>((i + 1) * count + j);
      indexes.push_back(v0);
      indexes.push_back(v1);
      indexes.push_back(v2);

      uint8_t v3 = static_cast<uint8_t>((i + 1) * count + j + 1);
      uint8_t v4 = static_cast<uint8_t>(i * count + j + 1);
      uint8_t v5 = static_cast<uint8_t>((i + 1) * count + j);

      indexes.push_back(v3);
      indexes.push_back(v4);
      indexes.push_back(v5);
    }
  }
}

vertex transformed_vertex(size_t x, size_t y, int r, int g, int b) {
  return {static_cast<double>(x), static_cast<double>(y),
          static_cast<double>(r), static_cast<double>(g),
          static_cast<double>(b)};
}

void renderer::on_frame_draw(sdl_canvas *canvas, int width, int height) {
  const color color = {255, 255, 255};
  const int size = 10;
  std::vector<vertex> vertexes;
  std::vector<size_t> indexrs;
  canvas->clear(color);
  generate_vertexes(width, height, size, vertexes);
  generate_indexes(size, indexrs);
  canvas->draw_triangles(vertexes, indexrs);
}

struct program_y : program {

  int mouse_x = -1;
  int mouse_y = -1;
  const int distace = 50;

  void set_uniforms(const uniforms &u) override {
    mouse_x = static_cast<int>(u.f0);
    mouse_y = static_cast<int>(u.f1);
  }
  vertex vertex_shader(const vertex &v_in) override {
    vertex out = v_in;
    if (mouse_x == -1 || mouse_y == -1) {
      return out;
    }
    int distance = static_cast<int>(std::sqrt(std::pow(mouse_x - v_in.x, 2) +
                                              std::pow((mouse_y - v_in.y), 2)));
    if (distance <= 200) {
      double t = static_cast<double>(distance) / 200;
      out.r = out.r * t;
      out.g = out.g * t;
      out.b = out.b * t;
    }
    return out;
  }

  bool is_depends(double h, double delta, double ri) {
    return ri >= (h - delta) && ri <= (h + delta);
  }

  struct color fragment_shader(const vertex &v_in) override {
    struct color out;
    const double delta = 0.4;
    const double h0 = 50;
    const double h1 = 70;
    const double h2 = 90;
    const double h3 = 110;
    const double h4 = 130;
    const double h5 = 150;
    const double h6 = 170;
    const double h7 = 190;
    const double h8 = 210;
    const double h9 = 230;

    double rd = v_in.r;
    double gd = v_in.g;
    double bd = v_in.b;

    double coef = 0.7;

    if (is_depends(h0, delta, rd) || is_depends(h1, delta, rd) ||
        is_depends(h2, delta, rd) || is_depends(h3, delta, rd) ||
        is_depends(h4, delta, rd) || is_depends(h5, delta, rd) ||
        is_depends(h6, delta, rd) || is_depends(h7, delta, rd) ||
        is_depends(h8, delta, rd) || is_depends(h9, delta, rd)) {

      coef = coef * (rd / (rd + delta));

      // draw green isoline
      out.r = static_cast<uint8_t>(rd * coef);
      out.g = static_cast<uint8_t>(gd * coef);
      out.b = static_cast<uint8_t>(bd * coef);
      return out;
    }

    out.r = static_cast<uint8_t>(rd);
    out.g = static_cast<uint8_t>(gd);
    out.b = static_cast<uint8_t>(bd);
    return out;
  }
};

void renderer::on_gl_created(sdl_canvas *canvas) {
  program_y *program = new program_y;
  canvas->set_program(program);
  std::cout << "On gl created" << std::endl;
}

void renderer::on_gl_destroyed() {
  std::cout << "On gl destroyed" << std::endl;
}
