#pragma once

#include "./render_utils/primitives.hxx"
#include <vector>

class canvas_base {
protected:
  int height;
  int width;
  std::vector<color> colors_buffer;
  struct program *program = nullptr;

public:
  canvas_base(int width = 480, int height = 320)
      : height(height), width(width),
        colors_buffer(static_cast<size_t>(width * height)) {}

  ~canvas_base() { delete program; }

  void clear(const color &);
  void set_color(std::vector<point> &, const color &);
  void draw_point(point, const color &);
  void draw_point(vertex &);

  std::vector<point> line_points(point, point);
  void draw_line(point, point, const color &);

  std::vector<point> triangle_points(point, point, point);
  void draw_triangle_stroke(point, point, point, const color &);

  void draw_triangles(std::vector<point> &, size_t, const color &);
  void draw_triangles(std::vector<point> &, std::vector<uint8_t> &,
                      const color &);
  void draw_triangles(std::vector<vertex> &, size_t, const color &);
  void draw_triangles(std::vector<vertex> &, std::vector<size_t> &);

  void draw_triangle(point, point, point, const color &);
  void draw_triangles(std::vector<vertex> &);

  void raster_line(vertex &, vertex &, std::vector<vertex> &);
  std::vector<vertex> raster_triangle(vertex &, vertex &, vertex &);
};
