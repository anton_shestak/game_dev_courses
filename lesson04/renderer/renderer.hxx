#pragma once

#include "../sdl_canvas/sdl_canvas.hxx"

class sdl_canvas;

struct i_renderer {
  virtual void on_gl_created(sdl_canvas *canvas) {}
  virtual void on_frame_draw(sdl_canvas *, int, int) {}
  virtual void on_gl_destroyed() {}
};
