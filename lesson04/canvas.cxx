#include "canvas.hxx"
#include "./render_utils/math_util.hxx"
#include <algorithm>
#include <exception>

void canvas_base::clear(const color &target_color) {
  size_t size = colors_buffer.size();
  for (unsigned int i = 0; i < size; i++) {
    colors_buffer[i] = target_color;
  }
}

void canvas_base::draw_point(point p, const color &color) {

  size_t position = static_cast<size_t>(width) * static_cast<unsigned>(p.y) +
                    static_cast<unsigned>(p.x);
  colors_buffer[position] = color;
}

void canvas_base::draw_point(vertex &v) {
  int x = static_cast<int>(v.x);
  int y = static_cast<int>(v.y);
  uint8_t r = static_cast<uint8_t>(v.r);
  uint8_t g = static_cast<uint8_t>(v.g);
  uint8_t b = static_cast<uint8_t>(v.b);
  color c{r, g, b};
  draw_point({x, y}, c);
}

void canvas_base::set_color(std::vector<point> &points, const color &col) {
  std::for_each(begin(points), end(points),
                [&](auto &point) { canvas_base::draw_point(point, col); });
}

std::vector<point> canvas_base::line_points(point p1, point p2) {
  std::vector<point> points;
  bool steep = false;
  if (std::abs(p1.x - p2.x) < std::abs(p1.y - p2.y)) {
    std::swap(p1.x, p1.y);
    std::swap(p2.x, p2.y);
    steep = true;
  }
  if (p1.x > p2.x) {
    std::swap(p1.x, p2.x);
    std::swap(p1.y, p2.y);
  }
  int dx = p2.x - p1.x;
  int dy = p2.y - p1.y;
  int derror2 = std::abs(dy) * 2;
  int error2 = 0;
  int y = p1.y;
  for (int x = p1.x; x <= p2.x; x++) {
    if (steep) {
      points.push_back({y, x});
    } else {
      points.push_back({x, y});
    }
    error2 += derror2;

    if (error2 > dx) {
      y += (p2.y > p1.y ? 1 : -1);
      error2 -= dx * 2;
    }
  }
  return points;
}

void canvas_base::draw_line(point p1, point p2, const color &col) {
  std::vector<point> points = canvas_base::line_points(p1, p2);
  set_color(points, col);
}

std::vector<point> canvas_base::triangle_points(point p1, point p2, point p3) {
  using namespace std;
  vector<point> points;

  for (auto [begin, end] : {pair{p1, p2}, pair{p2, p3}, pair{p3, p1}}) {
    for (auto point : line_points(begin, end)) {
      points.push_back(point);
    }
  }
  return points;
}

void canvas_base::draw_triangle_stroke(point p1, point p2, point p3,
                                       const color &col) {
  std::vector<point> points = triangle_points(p1, p2, p3);
  set_color(points, col);
}

void canvas_base::draw_triangle(point p1, point p2, point p3,
                                const color &col) {
  std::vector<point> points = {p1, p2, p3};
  // top is 0, bottom is 2 indxs
  std::sort(points.begin(), points.end(),
            [](const point &pp1, const point &pp2) { return pp1.y < pp2.y; });
  point middle = points[1];
  point top = points[0];
  point bottom = points[2];

  point temp_point = {bottom.x, middle.y};

  point cross_point = intersection(middle, temp_point, top, bottom);

  // draw top
  if (cross_point.y != top.y) {
    size_t lines_count = line_points(cross_point, top).size();
    if (lines_count > 0) {
      for (unsigned int i = 0; i < lines_count; i++) {
        double t = static_cast<double>(i) / lines_count;
        double localInterpolate = interpolate(cross_point.x, top.x, t);
        double localInterpolate1 = interpolate(cross_point.y, top.y, t);
        double localInterpolate2 = interpolate(middle.x, top.x, t);
        double localInterpolate3 = interpolate(middle.y, top.y, t);
        draw_line({static_cast<int>(localInterpolate),
                   static_cast<int>(localInterpolate1)},
                  {static_cast<int>(localInterpolate2),
                   static_cast<int>(localInterpolate3)},
                  {col});
      }
    }
  }

  // draw bottom
  if (cross_point.y != bottom.y) {
    size_t lines_count = line_points(bottom, cross_point).size();
    if (lines_count > 0) {
      for (unsigned int i = 0; i <= lines_count; i++) {
        double t = static_cast<double>(i) / lines_count;
        double localInterpolate = interpolate(bottom.x, cross_point.x, t);
        double localInterpolate1 = interpolate(bottom.y, cross_point.y, t);
        double localInterpolate2 = interpolate(bottom.x, middle.x, t);
        double localInterpolate3 = interpolate(bottom.y, middle.y, t);
        draw_line({static_cast<int>(localInterpolate),
                   static_cast<int>(localInterpolate1)},
                  {static_cast<int>(localInterpolate2),
                   static_cast<int>(localInterpolate3)},
                  {col});
      }
    }
  }
}

void canvas_base::draw_triangles(std::vector<point> &vers, size_t buf_size,
                                 const color &col) {
  std::vector<point> points;
  for (unsigned int i = 0; i < buf_size; i += 3) {
    point p1 = vers.at(i);
    point p2 = vers.at(i + 1);
    point p3 = vers.at(i + 2);
    draw_triangle_stroke(p1, p2, p3, col);
  }
}

void canvas_base::draw_triangles(std::vector<point> &vs,
                                 std::vector<uint8_t> &ins, const color &col) {
  for (unsigned int i = 0; i < ins.size(); i += 3) {
    point p1 = vs.at(ins.at(i));
    point p2 = vs.at(ins.at(i + 1));
    point p3 = vs.at(ins.at(i + 2));

    draw_triangle_stroke(p1, p2, p3, col);
  }
}

void canvas_base::draw_triangles(std::vector<vertex> &vs,
                                 std::vector<size_t> &ins) {

  size_t size = ins.size();
  for (unsigned int i = 0; i < size; i += 3) {
    vertex v0 = vs.at(ins.at(i));
    vertex v1 = vs.at(ins.at(i + 1));
    vertex v2 = vs.at(ins.at(i + 2));

    std::vector triangle{v0, v1, v2};
    draw_triangles(triangle);
  }
}

vertex interpolate_vertexes(vertex &v0, vertex &v1, double t) {
  return {interpolate(v0.x, v1.x, t), interpolate(v0.y, v1.y, t),
          interpolate(v0.r, v1.r, t), interpolate(v0.g, v1.g, t),
          interpolate(v0.b, v1.b, t)};
}

void canvas_base::raster_line(vertex &v0, vertex &v1,
                              std::vector<vertex> &f_buff) {
  int length = v1.x > v0.x ? static_cast<int>(v1.x - v0.x + 1)
                           : static_cast<int>(v0.x - v1.x + 1);
  if (length > 0) {
    for (int i = 0; i <= length; ++i) {
      double t = static_cast<double>(i) / length;
      vertex pixel = interpolate_vertexes(v0, v1, t);
      f_buff.push_back(pixel);
    }
  } else {
    f_buff.push_back(v0);
  }
}

std::vector<vertex> canvas_base::raster_triangle(vertex &v0, vertex &v1,
                                                 vertex &v2) {
  std::vector<vertex> f_shaders;
  std::vector<vertex> vs = {v0, v1, v2};
  // top is 0, bottom is 2 indxs
  std::sort(vs.begin(), vs.end(),
            [](const vertex &v1, const vertex &v2) { return v1.y < v2.y; });

  vertex middle = vs[1];
  vertex top = vs[0];
  vertex bottom = vs[2];

  point temp_point = {static_cast<int>(bottom.x), static_cast<int>(middle.y)};

  point top_p = {static_cast<int>(top.x), static_cast<int>(top.y)};
  point bottom_p = {static_cast<int>(bottom.x), static_cast<int>(bottom.y)};
  point middle_p = {static_cast<int>(middle.x), static_cast<int>(middle.y)};

  draw_triangle_stroke(top_p, bottom_p, middle_p, {0, 0, 0});

  point cross_point_p = intersection(middle_p, temp_point, top_p, bottom_p);

  size_t lines_count_bott_cross = line_points(bottom_p, cross_point_p).size();
  size_t lines_count_cross_top = line_points(cross_point_p, top_p).size();
  size_t lines_count_bottom_top = line_points(bottom_p, top_p).size();

  double t =
      static_cast<double>(lines_count_cross_top == 0 ? lines_count_bott_cross
                                                     : lines_count_cross_top) /
      (lines_count_bottom_top + 1);
  vertex cross_point = interpolate_vertexes(top, bottom, t);

  draw_line(cross_point_p, middle_p, {255, 0, 0});

  if (middle_p.y != top_p.y) {
    if (lines_count_cross_top > 0) {
      for (unsigned int i = 0; i < lines_count_cross_top; i++) {
        double t = static_cast<double>(i) / lines_count_cross_top;

        vertex vs = interpolate_vertexes(cross_point, top, t);
        vertex vf = interpolate_vertexes(middle, top, t);
        raster_line(vs, vf, f_shaders);
      }
    } else {
      raster_line(cross_point, top, f_shaders);
    }
  }

  if (middle_p.y != bottom_p.y) {
    if (lines_count_bott_cross > 0) {
      for (unsigned int i = 0; i <= lines_count_bott_cross; i++) {
        double t = static_cast<double>(i) / lines_count_bott_cross;

        vertex vs = interpolate_vertexes(bottom, cross_point, t);
        vertex vf = interpolate_vertexes(bottom, middle, t);
        raster_line(vs, vf, f_shaders);
      }
    } else {
      raster_line(bottom, cross_point, f_shaders);
    }
  }
  return f_shaders;
}

void canvas_base::draw_triangles(std::vector<vertex> &vers) {
  for (unsigned int i = 0; i < vers.size(); i += 3) {
    vertex v0 = vers.at(i);
    vertex v1 = vers.at(i + 1);
    vertex v2 = vers.at(i + 2);

    if (program == nullptr) {
      std::cout << "1" << std::endl;
      throw std::runtime_error("program is not inited!");
    }

    vertex v0p = program->vertex_shader(v0);
    vertex v1p = program->vertex_shader(v1);
    vertex v2p = program->vertex_shader(v2);

    std::vector<vertex> f_s = raster_triangle(v0p, v1p, v2p);
    for (vertex &v : f_s) {
      const color col = program->fragment_shader(v);
      draw_point({static_cast<int>(v.x), static_cast<int>(v.y)}, col);
    }
  }
}
