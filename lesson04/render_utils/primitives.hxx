#pragma once

#include <iostream>
#include <vector>

struct point;
using points = std::vector<point>;

#pragma pack(push, 1)
struct color {
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
};
#pragma pack(pop)

struct point {
  int x = 0;
  int y = 0;
};

struct vertex {
  double x = 0;
  double y = 0;
  double r = 0;
  double g = 0;
  double b = 0;
};

struct uniforms {
  double f0 = 0;
  double f1 = 0;
  double f2 = 0;
  double f3 = 0;
  double f4 = 0;
  double f5 = 0;
  double f6 = 0;
  double f7 = 0;
};

struct program {
  virtual ~program() = default;
  virtual void set_uniforms(const uniforms &) = 0;
  virtual vertex vertex_shader(const vertex &v_in) = 0;
  virtual color fragment_shader(const vertex &v_in) = 0;
};
