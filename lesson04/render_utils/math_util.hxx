#pragma once

#include "primitives.hxx"
#include <algorithm>

inline double interpolate(const double f0, const double f1, const double t) {
  return f0 + (f1 - f0) * t;
}

point intersection(point p1, point p2, point p3, point p4) {
  float x1 = p1.x, x2 = p2.x, x3 = p3.x, x4 = p4.x;
  float y1 = p1.y, y2 = p2.y, y3 = p3.y, y4 = p4.y;

  float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
  float pre = (x1 * y2 - y1 * x2), post = (x3 * y4 - y3 * x4);
  float x = (pre * (x3 - x4) - (x1 - x2) * post) / d;
  float y = (pre * (y3 - y4) - (y1 - y2) * post) / d;
  return {static_cast<int>(x), static_cast<int>(y)};
}
