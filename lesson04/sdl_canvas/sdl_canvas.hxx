#pragma once

#include "../canvas.hxx"
#include "../renderer/renderer.hxx"
#include <SDL.h>

class sdl_canvas : public canvas_base {
private:
  SDL_Window *sdl_window = nullptr;
  SDL_Renderer *sdl_renderer = nullptr;
  i_renderer *renderer = nullptr;

public:
  sdl_canvas(int widht, int heigh) : canvas_base(widht, heigh) {}
  void set_renderer(struct i_renderer &rend) { renderer = &rend; }
  void render();
  void set_program(struct program *prog) { program = prog; }

private:
  int init_sdl();
  int destroy_sdl();
  void save_image(const std::string &file_name);
};
