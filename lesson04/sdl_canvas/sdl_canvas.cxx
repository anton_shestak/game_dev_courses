#include "sdl_canvas.hxx"

#include <SDL.h>
#include <ctime>
#include <fstream>

void sdl_canvas::save_image(const std::string &file_name) {
  std::ofstream out_file;
  out_file.exceptions(std::ios_base::failbit);
  out_file.open(file_name, std::ios_base::binary);
  out_file << "P6\n" << width << ' ' << height << ' ' << 255 << '\n';
  std::streamsize buf_size =
      static_cast<std::streamsize>(sizeof(color) * colors_buffer.size());
  out_file.write(reinterpret_cast<const char *>(&colors_buffer[0]), buf_size);
}

void sdl_canvas::render() {
  init_sdl();
  if (renderer != nullptr) {
    renderer->on_gl_created(this);
  }
  bool continue_loop = true;
  while (continue_loop) {
    if (renderer != nullptr) {
      renderer->on_frame_draw(this, width, height);
    }
    SDL_Event e;
    int mouse_x = 0;
    int mouse_y = 0;
    while (SDL_PollEvent(&e)) {
      if (e.type == SDL_QUIT) {
        continue_loop = false;
        break;
      }
      if (e.key.type == SDL_KEYUP && e.key.keysym.sym == SDLK_SPACE) {
        std::string c_t = std::to_string(std::time(nullptr));
        save_image(c_t);
      }

      if (e.type == SDL_MOUSEMOTION) {
        mouse_x = e.motion.x;
        mouse_y = e.motion.y;
        program->set_uniforms(
            {static_cast<double>(mouse_x), static_cast<double>(mouse_y)});
      }
    }

    void *pixels = &colors_buffer[0];
    const int depth = sizeof(color) * 8;
    const int pitch = width * static_cast<int>(sizeof(color));
    const int rmask = 0x000000ff;
    const int gmask = 0x0000ff00;
    const int bmask = 0x00ff0000;
    const int amask = 0;

    SDL_Surface *bitmapSurface = SDL_CreateRGBSurfaceFrom(
        pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
    if (bitmapSurface == nullptr) {
      std::cerr << SDL_GetError() << std::endl;
    }
    SDL_Texture *bitmapTex =
        SDL_CreateTextureFromSurface(sdl_renderer, bitmapSurface);
    if (bitmapTex == nullptr) {
      std::cerr << SDL_GetError() << std::endl;
    }
    SDL_FreeSurface(bitmapSurface);

    SDL_RenderClear(sdl_renderer);
    SDL_RenderCopy(sdl_renderer, bitmapTex, nullptr, nullptr);
    SDL_RenderPresent(sdl_renderer);

    SDL_DestroyTexture(bitmapTex);
  }
  destroy_sdl();
}

int sdl_canvas::init_sdl() {
  int init_result = SDL_Init(SDL_INIT_EVERYTHING);
  if (init_result != 0) {
    std::cerr << SDL_GetError() << std::endl;
  }

  sdl_window = SDL_CreateWindow("PRESS SPACE FOR SAVE BUFFER",
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                width, height, SDL_WINDOW_OPENGL);

  if (sdl_window == nullptr) {
    std::cerr << SDL_GetError() << std::endl;
    return 1;
  }

  sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
  if (sdl_renderer == nullptr) {
    std::cerr << SDL_GetError() << std::endl;
    return 1;
  }
  return 0;
}

int sdl_canvas::destroy_sdl() {
  if (sdl_renderer == nullptr || sdl_renderer == nullptr) {
    std::cerr << "Destroy SDL error!" << std::endl;
    return 1;
  }
  SDL_DestroyRenderer(sdl_renderer);
  SDL_DestroyWindow(sdl_window);
  SDL_Quit();
  if (renderer != nullptr) {
    renderer->on_gl_destroyed();
  }
  return 0;
}
