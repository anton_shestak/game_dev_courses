#pragma once

#include <iostream>

struct vertex {
  float x = 0.f;
  float y = 0.f;
  float z = 0.f;
};

inline std::istream &operator>>(std::istream &is, vertex &v) {
  is >> v.x;
  is >> v.y;
  is >> v.z;
  return is;
}

struct triangle {
  triangle() {
    v[0] = vertex();
    v[1] = vertex();
    v[2] = vertex();
  }
  vertex v[3];
};

inline std::istream &operator>>(std::istream &is, triangle &t) {
  is >> t.v[0];
  is >> t.v[1];
  is >> t.v[2];
  return is;
}
