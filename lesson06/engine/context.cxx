#include <exception>

#include "context.hxx"

#define STATUS_SUCCESS 0
#define STATUS_FAILURE 1

int context_SDL::check_SDL_version() {
  SDL_version link_version, compile_version;

  SDL_VERSION(&compile_version)
  SDL_GetVersion(&link_version);

  using namespace std;
  if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(link_version.major,
                                            link_version.minor,
                                            link_version.patch)) {
    cerr << "Linking version and Compile version is missmatched." << endl;
    return STATUS_FAILURE;
  }

  cout << "Linking version:" << link_version << "\n"
       << "Compiling version:" << compile_version << endl;

  return STATUS_SUCCESS;
}

int context_SDL::check_GL_version(int majore_version, int minore_version) {
  int mj_resul =
      SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majore_version);
  int mn_resul =
      SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minore_version);

  using namespace std;
  if (mj_resul != STATUS_SUCCESS) {
    cerr << "Invalid major version." << endl;
    return STATUS_FAILURE;
  }
  if (mn_resul != STATUS_SUCCESS) {
    cerr << "Invalid minore version." << endl;
    return STATUS_FAILURE;
  }

  if (majore_version < 2) {
    clog << "current context opengl version: " << majore_version << '.'
         << minore_version << '\n'
         << "need openg version at least: 2.1\n"
         << std::flush;
    throw runtime_error("Opengl version too low");
  }
  return STATUS_SUCCESS;
}

int context_SDL::init_SDL() {
  int result = SDL_Init(SDL_INIT_EVERYTHING);
  if (result != 0) {
    using namespace std;
    cerr << "SDL initialization error." << endl;
    return STATUS_FAILURE;
  }
  return STATUS_SUCCESS;
}

void context_SDL::validate_status(int status) {
  if (STATUS_FAILURE == status) {
    throw std::runtime_error("INVALID STATUS");
  }
}

void context_SDL::create_gl_window(const char *title, int height, int width) {
  context_window =
      SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       width, height, ::SDL_WINDOW_OPENGL);
  if (context_window == nullptr) {
    const char *err_message = SDL_GetError();
    using namespace std;
    cerr << err_message << endl;
    SDL_Quit();
    throw runtime_error("Can't create SDL window.");
  }
}

void context_SDL::create_gl_context(int majore_version, int minore_version) {
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, majore_version);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minore_version);

  SDL_GLContext gl_context = SDL_GL_CreateContext(context_window);
  // if context wasn't created fromg specified gl versions we use default from
  // SDL
  if (gl_context == nullptr) {
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    gl_context = SDL_GL_CreateContext(context_window);
  }
  if (gl_context == nullptr) {
    throw std::runtime_error("GL context can't be created.");
  }
  int status = check_GL_version(majore_version, minore_version);
  validate_status(status);
}

void context_SDL::render() {
  if (renderer != nullptr) {
    renderer->on_game_created();
  }
  bool is_window_valid = true;
  while (is_window_valid) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
        is_window_valid = false;
      }
    }
    if (renderer != nullptr) {
      renderer->on_draw_frame();
    }
    SDL_GL_SwapWindow(context_window);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }
  if (renderer != nullptr) {
    renderer->on_game_destroyed();
  }
}
