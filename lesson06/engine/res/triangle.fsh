#version 300 es
precision mediump float;
out vec4 frag_color;
in vec4 v_position;
     
void main()
{
	if (v_position.z >= 0.0)
        {
        	float light_green = 0.5 + v_position.z / 2.0;
                frag_color = vec4(0.0, light_green, 0.0, 1.0);
         } else {         
         	float color = 0.5 - (v_position.z / -2.0);
                frag_color = vec4(color, 0.0, 0.0, 1.0);
         }
}
