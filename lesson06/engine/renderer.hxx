#pragma once

#include <assert.h>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string_view>
#include <vector>

#include <SDL.h>
#include <SDL_opengles2.h>

#include "./utils/file_util.hxx"
#include "primitives.hxx"

const std::string vertexes = "engine/res/vertexes.txt";
const std::string path_vertex = "engine/res/triangle.vsh";
const std::string path_fragment = "engine/res/triangle.fsh";

#define CHECK_GL_STATUS()                                                      \
  {                                                                            \
    const GLenum err = glGetError();                                           \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
      case GL_INVALID_ENUM:                                                    \
        std::cerr << "GL_INVALID_ENUM" << std::endl;                           \
        break;                                                                 \
      case GL_INVALID_VALUE:                                                   \
        std::cerr << "GL_INVALID_VALUE" << std::endl;                          \
        break;                                                                 \
      case GL_INVALID_OPERATION:                                               \
        std::cerr << "GL_INVALID_OPERATION" << std::endl;                      \
        break;                                                                 \
      case GL_INVALID_FRAMEBUFFER_OPERATION:                                   \
        std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;          \
        break;                                                                 \
      case GL_OUT_OF_MEMORY:                                                   \
        std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                          \
        break;                                                                 \
      }                                                                        \
      std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__ << ')'   \
                << std::endl;                                                  \
      assert(false);                                                           \
    }                                                                          \
  }

template <typename T>
static void load_gl_func(const char *func_name, T &result) {
  void *gl_pointer = SDL_GL_GetProcAddress(func_name);
  if (nullptr == gl_pointer) {
    throw std::runtime_error(std::string("can't load GL function") + func_name);
  }
  result = reinterpret_cast<T>(gl_pointer);
}

struct gl_renderer {

  GLuint program_id_ = 0;

  PFNGLCREATESHADERPROC glCreateShader = nullptr;
  PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
  PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
  PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
  PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
  PFNGLDELETESHADERPROC glDeleteShader = nullptr;
  PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
  PFNGLATTACHSHADERPROC glAttachShader = nullptr;
  PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
  PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
  PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
  PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
  PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
  PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
  PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
  PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
  PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;
  PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
  PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
  PFNGLGENVERTEXARRAYSOESPROC glGenVertexArrays = nullptr;
  PFNGLBINDVERTEXARRAYOESPROC glBindVertexArray = nullptr;
  PFNGLBUFFERDATAPROC glBufferData = nullptr;

  void on_game_created() {
    load_gl_func("glCreateShader", glCreateShader);
    load_gl_func("glShaderSource", glShaderSource);
    load_gl_func("glCompileShader", glCompileShader);
    load_gl_func("glGetShaderiv", glGetShaderiv);
    load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
    load_gl_func("glDeleteShader", glDeleteShader);
    load_gl_func("glCreateProgram", glCreateProgram);
    load_gl_func("glAttachShader", glAttachShader);
    load_gl_func("glBindAttribLocation", glBindAttribLocation);
    load_gl_func("glLinkProgram", glLinkProgram);
    load_gl_func("glGetProgramiv", glGetProgramiv);
    load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
    load_gl_func("glDeleteProgram", glDeleteProgram);
    load_gl_func("glUseProgram", glUseProgram);
    load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
    load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
    load_gl_func("glValidateProgram", glValidateProgram);
    load_gl_func("glBindBuffer", glBindBuffer);
    load_gl_func("glGenBuffers", glGenBuffers);
    load_gl_func("glGenVertexArrays", glGenVertexArrays);
    load_gl_func("glBindVertexArray", glBindVertexArray);
    load_gl_func("glBufferData", glBufferData);

    int a;
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &a);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &a);
    std::cout << a << std::endl;

    // generate VBO
    GLuint vboId;
    glGenBuffers(1, &vboId);
    glBindBuffer(GL_ARRAY_BUFFER, vboId);

    GLuint vert_shader = glCreateShader(GL_VERTEX_SHADER);
    const char *vertex_source = parce_shader(path_vertex);
    glShaderSource(vert_shader, 1, &vertex_source, nullptr);
    CHECK_GL_STATUS()
    glCompileShader(vert_shader);
    GLint compiled_status = 0;
    glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_GL_STATUS()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(vert_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_GL_STATUS()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(vert_shader, info_len, nullptr, info_chars.data());
      CHECK_GL_STATUS()
      glDeleteShader(vert_shader);
      CHECK_GL_STATUS()

      std::string shader_type_name = "vertex";
      std::cerr << "Error compiling shader(vertex)\n"
                << vertex_source << "\n"
                << info_chars.data();
    }

    CHECK_GL_STATUS()
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

    char *fragment_source = parce_shader(path_fragment);
    glShaderSource(fragment_shader, 1, &fragment_source, nullptr);
    CHECK_GL_STATUS()
    glCompileShader(fragment_shader);
    compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    CHECK_GL_STATUS()
    if (compiled_status == 0) {
      GLint info_len = 0;
      glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
      CHECK_GL_STATUS()
      std::vector<char> info_chars(static_cast<size_t>(info_len));
      glGetShaderInfoLog(fragment_shader, info_len, nullptr, info_chars.data());
      CHECK_GL_STATUS()
      glDeleteShader(fragment_shader);
      CHECK_GL_STATUS()

      std::cerr << "Error compiling shader(fragment)\n"
                << fragment_source << "\n"
                << info_chars.data();
    }
    CHECK_GL_STATUS()
    program_id_ = glCreateProgram();
    CHECK_GL_STATUS()
    glAttachShader(program_id_, vert_shader);
    CHECK_GL_STATUS()
    glAttachShader(program_id_, fragment_shader);
    CHECK_GL_STATUS()

    glBindAttribLocation(program_id_, 0, "a_position");
    CHECK_GL_STATUS()
    glLinkProgram(program_id_);

    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    CHECK_GL_STATUS()
    if (linked_status == 0) {
      GLint infoLen = 0;
      glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
      CHECK_GL_STATUS()
      std::vector<char> infoLog(static_cast<size_t>(infoLen));
      glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
      CHECK_GL_STATUS()
      std::cerr << "Error linking program:\n" << infoLog.data();
      glDeleteProgram(program_id_);
      CHECK_GL_STATUS()
    }
    glUseProgram(program_id_);
    CHECK_GL_STATUS()
    glEnable(GL_DEPTH_TEST);
  }

  vertex blend_vertext(vertex &v1, vertex &v2, float t) {
    vertex v;
    float t2 = 1.0f - t;
    v.x = t2 * v1.x + t * v2.x;
    v.y = t2 * v1.y + t * v2.y;
    v.z = t2 * v1.z + t * v2.z;
    return v;
  }

  triangle blend_triangle(triangle &t1, triangle &t2, float t) {
    triangle tr;
    tr.v[0] = blend_vertext(t1.v[0], t2.v[0], t);
    tr.v[1] = blend_vertext(t1.v[1], t2.v[1], t);
    tr.v[2] = blend_vertext(t1.v[2], t2.v[2], t);
    return tr;
  }

  triangle get_transformed_teianles(std::vector<triangle> &triangles) {
    float t = std::sin(get_time_from_init()) * 0.5f + 0.5f;
    return blend_triangle(triangles.at(0), triangles.at(1), t);
  }

  void on_draw_frame() {

    std::vector<triangle> *vertexesVector = parce_vertexes(vertexes);

    triangle temp = get_transformed_teianles(*vertexesVector);

    //    for (triangle triangle : *vertexesVector) {
    //      glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle.v,
    //                   GL_STATIC_DRAW);

    //      glEnableVertexAttribArray(0);
    //      glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
    //                            reinterpret_cast<void *>(0));
    //      glDrawArrays(GL_TRIANGLES, 0, 3);
    //    }

    glBufferData(GL_ARRAY_BUFFER, sizeof(temp), temp.v, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                          reinterpret_cast<void *>(0));
    glDrawArrays(GL_TRIANGLES, 0, 3);
  }

  float get_time_from_init() { return SDL_GetTicks() * 0.001f; }

  void on_game_destroyed() {}
};
