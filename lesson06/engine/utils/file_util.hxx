#pragma once

#include <string>
#include <vector>

#include "../primitives.hxx"

char *parce_shader(const std::string &path);

std::vector<triangle> *parce_vertexes(const std::string &path);
