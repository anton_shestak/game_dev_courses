cmake_minimum_required(VERSION 3.13)
project(hello CXX)

add_executable(hello-static main.cxx)
target_link_libraries(hello-static LINK_PUBLIC welcomer-lib-static)
target_link_options(hello-static PRIVATE -static)
target_compile_features(hello-static PUBLIC cxx_std_17)

add_executable(hello-shared main.cxx)
target_link_libraries(hello-shared LINK_PUBLIC welcomer-lib-shared)
target_compile_features(hello-shared PUBLIC cxx_std_17)
