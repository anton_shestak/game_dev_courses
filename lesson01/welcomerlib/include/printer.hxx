/// print to stdout "Hi, {user_name}" and return true on success
bool greet_user(const char *user_name);

/// p rint to stdout "Hi, {user_login}" and return true in success
/// note: user_loging is a user which logged in
bool greet_logged_in_user();
