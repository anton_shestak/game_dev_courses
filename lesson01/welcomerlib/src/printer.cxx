#include <iostream>

bool greet_user(const char *username) {
  using namespace std;
  cout << "Hi";
  if ((username != nullptr) && (username[0] != '\0')) {
    cout << ", " << username;
  }
  cout << "!" << endl;
  return cout.good() ? EXIT_SUCCESS : EXIT_FAILURE;
}

bool greet_logged_in_user() { return greet_user(getenv("USER")); }
