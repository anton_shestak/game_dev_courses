#include "printer.hxx"
#include <gtest/gtest.h>

// test printer of empty chars
TEST(printer_test, empty_string) {
  testing::internal::CaptureStdout();
  greet_user("");
  std::string output = testing::internal::GetCapturedStdout();
  EXPECT_EQ("Hi!\n", output);
}

// test printer of nullptr
TEST(printer_test, null_ptr_arg) {
  testing::internal::CaptureStdout();
  greet_user(nullptr);
  std::string output = testing::internal::GetCapturedStdout();
  EXPECT_EQ("Hi!\n", output);
}

// test printer with corret name
TEST(printer_test, correct_name) {
  testing::internal::CaptureStdout();
  greet_user("some_name");
  std::string output = testing::internal::GetCapturedStdout();
  EXPECT_EQ("Hi, some_name!\n", output);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
