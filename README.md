# Small game development project 

Repository represents the step-by-step diving into a game programming from junior level programming issues solving to realization small game engine for 2d games. 
Lessons are structured by folders. Each folder has a suffix wich respective to the lesson number. 

During the repository expanding, each new lesson task should be passed through the testing. Docker CI was involved for this reason.
The status of test passing you can find below.

# Build status

| Lesson | Status |
|---|---|
| 01 |![Linux x64](https://img.shields.io/bitbucket/pipelines/anton_shestak/game_dev_courses.svg)|