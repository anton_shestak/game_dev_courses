FROM ubuntu:19.04
RUN apt-get update
RUN apt-get install -y g++ gcc make cmake ninja-build git \
&& cd /home && mkdir prog && cd /prog \
&& git clone https://github.com/google/googletest.git \
&& cd googletest && mkdir build && cd build &&  cmake .. && make && make install

